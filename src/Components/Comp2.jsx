import './Comp2.css'
import photo from '../assets/photo.jpg'

const Comp2 = () => {
  return (
    <div className="body">
        <div className="photo">
            <img src={photo} alt="" />
        </div>
        <div className="data">
        <h2>Mis Datos</h2>
            <p id='name'>Lima Sánchez Daniel</p>
            <p>Edad: 21 <br />
            Carrera: Ingenieria En Sistemas Computacionales <br />
            Matricula: 19680178 <br />
            Correo: 19680178@cuautla.tecnm.mx <br />
            Telefono: 7352495576 <br />
            Hobbit: Videojuegos
            </p>
        </div>
    </div>
  )
}

export default Comp2