import './Comp3.css'
import Facebook from '../assets/facebook.png'
import Instagram from '../assets/instagram.png'
import Twitter from '../assets/twitter.png'
import GitLab from '../assets/gitlab.png'
import WhatsApp from '../assets/whatsapp.png'

const Comp3 = () => {
  return (
    <div className="social">
        <a href="https://www.facebook.com/profile.php?id=100006638441598" target="_blank">
            <img src={Facebook} alt="" />
        </a>
        <a href="https://www.instagram.com/danny_ls15/" target="_blank">
            <img src={Instagram} alt="" />
        </a>
        <a href="https://twitter.com/Daniel446DL" target="_blank">
            <img src={Twitter} alt="" />
        </a>
        <a href="https://gitlab.com/dani.ls15" target="_blank">
            <img src={GitLab} alt="" />
        </a>
        <a href="https://wa.me/+527352495576" target="_blank">
            <img src={WhatsApp} alt="" />
        </a>
        <h2>&copy; Danny 2023</h2>
    </div>
  )
}

export default Comp3