import Comp1 from './Components/Comp1'
import Comp2 from './Components/Comp2'
import Comp3 from './Components/Comp3'

function App() {

  return (
    <div className="App">
      <Comp1/>
      <Comp2/>
      <Comp3/>
    </div>
  )
}

export default App
